import os, sqlite3
from bottle import route, run, request, redirect, debug, static_file, url
from bottle import TEMPLATE_PATH, jinja2_template as template
import bottle

from datetime import datetime

app = bottle.app()

TEMPLATE_PATH.append("./templates")

# トップページ
@app.route('/')
def index():
    return template('index.html')

# ドロップカードリスト
@app.route('/drop_list')
def drop_list():
    conn = sqlite3.connect('db/card.db')
    c = conn.cursor()
    c.execute("SELECT id,name,type,get_map,satelite_no,place_name,get_timestamp FROM drop_card ORDER BY id")
    card_list = []
    for row in c.fetchall():
        card_list.append({
            'id': row[0],
            'name': row[1],
            'type': row[2],
            'get_map': row[3],
            'satelite_no': row[4],
            'place_name': row[5],
            'get_timestamp': row[6]
        })
    conn.close()
    return template("drop_list.html", card_list=card_list)

# 建造カードリスト
@app.route('/build_list')
def build_list():
    conn = sqlite3.connect('db/card.db')
    c = conn.cursor()
    c.execute("SELECT id,name,type,fuel,ammo,steel,bauxite,satelite_no,place_name,get_timestamp FROM build_card ORDER BY id")
    card_list = []
    for row in c.fetchall():
        card_list.append({
            'id': row[0],
            'name': row[1],
            'type': row[2],
            'fuel': row[3],
            'ammo': row[4],
            'steel': row[5],
            'bauxite': row[6],
            'satelite_no': row[7],
            'place_name': row[8],
            'get_timestamp': row[9]
        })
    conn.close()
    return template("build_list.html", card_list=card_list)


# ドロップカード登録
@app.route('/drop_add', method=["GET", "POST"])
def add_drop_card():
    if request.method == "POST":
        # メソッドがPOSTならDBに登録する
        # 日本語を受け取る場合はrequest.POST.getunicodeで受け取る
        card_name = request.POST.getunicode("card_name")
        card_type = request.POST.getunicode("card_type")
        card_get_map = request.POST.getunicode("card_get_map")
        card_satelite_no = request.POST.getunicode("card_satelite_no")
        card_place_name = request.POST.getunicode("card_place_name")
        card_get_timestamp = datetime.now().strftime('%Y-%m-%d_%H:%M:%S')
        conn = sqlite3.connect('db/card.db')
        c = conn.cursor()
        # 現在の最大ID取得
        new_id = c.execute("SELECT max(id) + 1 FROM drop_card").fetchone()[0]
        c.execute("INSERT INTO drop_card VALUES(?,?,?,?,?,?,?)", (new_id, card_name, card_type, card_get_map, card_satelite_no, card_place_name, card_get_timestamp))
        conn.commit()
        conn.close()
        # return "SUCCESS"
        return redirect("drop_list")
    else:
        # メソッドがGETならフォーム表示
        return template("drop_add.html")

# 建造カード登録
@app.route('/build_add', method=["GET", "POST"])
def add_build_card():
    if request.method == "POST":
        # メソッドがPOSTならDBに登録する
        # 日本語を受け取る場合はrequest.POST.getunicodeで受け取る
        card_name = request.POST.getunicode("card_name")
        card_type = request.POST.getunicode("card_type")
        card_fuel = request.POST.getunicode("card_fuel")
        card_ammo = request.POST.getunicode("card_ammo")
        card_steel = request.POST.getunicode("card_steel")
        card_bauxite = request.POST.getunicode("card_bauxite")
        card_satelite_no = request.POST.getunicode("card_satelite_no")
        card_place_name = request.POST.getunicode("card_place_name")
        card_get_timestamp = datetime.now().strftime('%Y-%m-%d_%H:%M:%S')
        conn = sqlite3.connect('db/card.db')
        c = conn.cursor()
        # 現在の最大ID取得
        new_id = c.execute("SELECT max(id) + 1 FROM build_card").fetchone()[0]
        c.execute("INSERT INTO build_card VALUES(?,?,?,?,?,?,?,?,?,?)", (new_id, card_name, card_type,
            card_fuel, card_ammo, card_steel, card_bauxite,
            card_satelite_no, card_place_name, card_get_timestamp))
        conn.commit()
        conn.close()
        # return "SUCCESS"
        return redirect("build_list")
    else:
        # メソッドがGETならフォーム表示
        return template("build_add.html")

# ドロップカード削除
@app.route("/drop_del/<card_id:int>")
def del_item(card_id):
    conn = sqlite3.connect('db/card.db')
    c = conn.cursor()
    # 指定されたcard_idを元にDBのデータを削除
    c.execute("delete from drop_card where id=?", (card_id,))
    conn.commit()
    conn.close()
    # 処理終了後、一覧画面に戻す
    return redirect("/drop_list")

# 建造カード削除
@app.route("/build_del/<card_id:int>")
def del_item(card_id):
    conn = sqlite3.connect('db/card.db')
    c = conn.cursor()
    # 指定されたcard_idを元にDBのデータを削除
    c.execute("delete from build_card where id=?", (card_id,))
    conn.commit()
    conn.close()
    # 処理終了後、一覧画面に戻す
    return redirect("/build_list")

# CSS読み込み可能にする
@app.route('/static/<filepath:path>', name='static_file')
def static(filepath):
    return static_file(filepath, root="./static")

if __name__=='__main__':
    app.run(host='localhost', port=8090, reloader=True)
