#!/usr/bin/env python3
# coding=utf-8

import sqlite3

# card.dbと接続(なければ作る)
conn = sqlite3.connect('db/card.db')
c = conn.cursor()

# Create table
# ドロップ(drop_card), 建造(build_card), 貰った(got_card)

# ID, 艦娘名, カード種類, 入手マップ, 台番号, 店名, 入手時刻
c.execute("create table drop_card(id, name, type, get_map, satelite_no, place_name, get_timestamp)")

# ID, 艦娘名, カード種類, 燃料(fuel), 弾薬(ammo), 鋼材(steel), ボーキ(bauxite), 台番号, 店名, 入手時刻
c.execute("create table build_card(id, name, type, fuel, ammo, steel, bauxite, satelite_no, place_name, get_timestamp)")

# Insert a row of data
c.execute("insert into drop_card values(1,'ああ', 'ノーマル', '1-1', '16', 'ああああ', '2017-06-09_00:00:00')")
c.execute("insert into build_card values(1,'ああ', 'ノーマル', '321', '321', '321', '321', '16', 'ああああ', '2017-06-09_00:00:00')")

# Save (commit) the changes
conn.commit()

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
conn.close()
